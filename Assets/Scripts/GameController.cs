﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class GameController : MonoBehaviour {

	public Text scoreTxt;
	public Text gameOverTxt;
	public GameObject objectStartQuit;
	public GameObject objectHighSocre;
	public SpawnRow mSpawnRow;
	private AudioSource mAudio;

	public bool isGameOver;
	private int currScore;
	private int scoreToWin = 20;
	private bool didIWin;

	void NewGame() {
		ResetGame();
	}
		
	public void GotOne() {
		currScore++;
		scoreTxt.text =  "" + currScore;
	}

	public void GameOver(bool win) {
		isGameOver = true;
		didIWin = win;
		gameOverTxt.text = (didIWin) ? "You won!" : "Game Over!";

		mAudio.Stop ();

		objectHighSocre.SetActive (true);
	}


	public void ResetGame() {

		objectStartQuit.SetActive (false);
		objectHighSocre.SetActive (false);

		gameOverTxt.text = "";
		isGameOver = false;
		currScore = 0;
		scoreTxt.text = "--";
		gameOverTxt.text = "";
		mAudio.Play ();

	}

	void Start () {
		mAudio = GetComponent<AudioSource> ();
		NewGame();
	}

	public void ResetNewGame()
	{
		NewGame();
		mSpawnRow.ResetRow();
	}

	public void QuitApp()
	{
		#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}

	public void LoadHighScore()
	{
		int highScore = PlayerPrefs.GetInt("high_score");
		scoreTxt.text = "" + highScore;
		gameOverTxt.text = "High Score";

		objectHighSocre.SetActive (false);
		objectStartQuit.SetActive (true);
	}

	public void SaveHighScore()
	{
		objectHighSocre.SetActive (false);
		objectStartQuit.SetActive (true);

		gameOverTxt.text = "Game Over!";
		PlayerPrefs.SetInt ("high_score", currScore);
	}
}
