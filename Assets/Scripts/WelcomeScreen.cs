﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class WelcomeScreen : MonoBehaviour {

	public void StartGame()
	{
		#if (UNITY_5_2 || UNITY_5_1 || UNITY_5_0)
			Application.LoadLevel(Application.loadedLevel + 1);
		#else // UNITY_5_3 or above
			UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex+1);
		#endif
	}

	public void QuitApp()
	{
		#if UNITY_EDITOR
			EditorApplication.Exit(0);
		#else
			Application.Quit();
		#endif
	}
}
