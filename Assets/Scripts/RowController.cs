﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class RowController : MonoBehaviour {

	public float downwardSpeed = 0;
	public GameObject[] rowList;

	private int countScore = 0;
	private float yPosReset = 0f;

	private SpawnRow spawnRow;

	void OnSpeedUpdate (SpawnRow row) 
	{
		transform.Translate (Vector3.down * row.MovingSpeed);
		if(transform.position.y <= yPosReset) 
		{
			spawnRow.SetScore ();
			Reset();
		}
	}



	public SpawnRow ProSpawnRow
	{
		set
		{
			spawnRow = value;
			spawnRow.onSpeedUpdate += OnSpeedUpdate;
			yPosReset = spawnRow.yPositionToReset;
		}
	}

	public void SetPosition(Vector3 position)
	{
		transform.position = position;
	}

	public void SetParent(Transform parent)
	{
		transform.parent = parent;
	}

	public void DisplayBlock(string arrDisplay)
	{
		arrDisplay = arrDisplay.Trim ();
		for (int i = 0; i < arrDisplay.Length; i++) 
		{
			if (arrDisplay [i].Equals ('1')) 
			{
				rowList [i].SetActive (true);
			} else {
				rowList [i].SetActive (false);
			}
		}
	}

	public void Reset()
	{
		SetPosition(spawnRow.GetLastPosition(this));
		DisplayBlock(spawnRow.GetRandomDisplayBlock ());
	}
}
