﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float moveSpeed;
	public float jumpHeight;
	public float groundRadius;

	public LayerMask whatIsGround;
	public GameController mGameController;

	private float originSpeed;
	private bool grounded;
	private bool moveAble;

	private Rigidbody mRigidbody;
	[SerializeField]
	private SpawnRow mSpawnRow;
	private AudioSource mAudio;

	void Start() 
	{
		mAudio = GetComponent<AudioSource>();
		mRigidbody = GetComponent<Rigidbody>();
		originSpeed = moveSpeed;
		grounded = true;
		moveAble = true;

		mSpawnRow.onSpeedUpdate += OnSpeedUpdate;
	}

	void OnSpeedUpdate(SpawnRow row)
	{
		if(grounded)
			transform.Translate (Vector3.down * row.MovingSpeed);
	}

	void FixedUpdate() 
	{
		
		if (Input.GetKeyDown(KeyCode.W) && grounded) 
		{
			Jump (jumpHeight);
		}

		if (Input.GetKey(KeyCode.A)) 
		{
			Move (-moveSpeed);
		} 
		
		if (Input.GetKey (KeyCode.D)) 
		{
			Move (moveSpeed);
		} 
	}

	public void SetOriginSpeed()
	{
		moveSpeed = originSpeed;
	}

	public void Move(float speed)
	{
		if(moveAble)
			mRigidbody.velocity = new Vector3 (speed, mRigidbody.velocity.y, 0);
	}

	public void Jump(float height)
	{
		grounded = false;
		mAudio.Play();
		mRigidbody.velocity = new Vector3(mRigidbody.velocity.x, height, 0);
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Bottom") {
			grounded = true;
			moveAble = true;
			mRigidbody.velocity = new Vector3 (mRigidbody.velocity.x, 0, 0);	
		} 
		else 
		{
			moveAble = false;
		}

		if (col.gameObject.name.Equals ("GameBoundary")) 
		{
			col.gameObject.GetComponent<AudioSource> ().Play ();
			mGameController.GameOver (false);
		}
	}

	void OnCollisionExit(Collision col) 
	{
		grounded = false;
		moveAble = true;
	}

	void OnCollisionStay(Collision col) 
	{
		if (col.gameObject.tag == "Bottom") 
		{
			grounded = true;
			transform.position = new Vector3 (transform.position.x, transform.position.y + 0.01f, 0); 
		} 
		else 
		{
			moveAble = false;
		}
	}
}
