﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnRow : MonoBehaviour {

	public int spawnNumber = 0;
	public float rowGap = 0f;
	public float yPositionToReset = 0;

	public TextAsset displayBlockFile;
	public Vector3 initRowPosition = Vector3.zero;
	public GameController mGameController;
	public RowController defaultRow;

	public delegate void OnSpeedUpdate (SpawnRow spanwRow);
	public event OnSpeedUpdate onSpeedUpdate;

	private RowController[] listRowController;
	private RowController LastRow;
	private GameObject rowPrefab;
	private string[] displayBlockText;

	[SerializeField]
	private float dropSpeed = 0.01f;
	public float MovingSpeed {
		get
		{ 
			return dropSpeed;
		}
		private set
		{ 
			dropSpeed = value;
		}
	}

	void FixedUpdate()
	{	
		if (!mGameController.isGameOver) 
		{
			if (onSpeedUpdate != null)
				onSpeedUpdate (this);
		}
	}

	void Start () 
	{
		GenerateNoDuplicateRandom();
		rowPrefab = Resources.Load("Prefabs/Row") as GameObject;
		InitializeRows ();
	}

	private void InitializeRows()
	{
		displayBlockText = displayBlockFile.text.Split('\n');

		listRowController = new RowController[spawnNumber + 1];
		listRowController[0] = defaultRow;

		SetPropertiesToRowController (defaultRow, initRowPosition);

		Vector3 lastPosition = initRowPosition;

		for (int i = 1; i < listRowController.Length; i++) 
		{
			lastPosition.y += rowGap;
			GameObject rowGameObject = (GameObject)Instantiate (rowPrefab);

			listRowController [i] = rowGameObject.GetComponent<RowController> ();

			listRowController [i].DisplayBlock (GetRandomDisplayBlock());

			SetPropertiesToRowController (listRowController [i], lastPosition);
		}
	}

	private void SetPropertiesToRowController(RowController rowController, Vector3 position)
	{
		rowController.ProSpawnRow = this;
		rowController.SetPosition(position);
		rowController.SetParent(transform);
		LastRow = rowController;
	}

	public Vector3 GetLastPosition(RowController resetRow)
	{
		Vector3 lastPosition = LastRow.transform.position;
		lastPosition.y += rowGap;
		LastRow = resetRow;
		return lastPosition;
	}

	public string GetRandomDisplayBlock()
	{
		int id = noDupNumber[index];
		string result = displayBlockText[id];
		index++;
		if (index >= noDupNumber.Length) 
		{
			
			lastIndex = noDupNumber [noDupNumber.Length-1];
			index = 0;
			GenerateNoDuplicateRandom ();
		}
		return result;
	}

	private int index = 0;
	private int[] noDupNumber;
	private int lastIndex = 10;

	public void GenerateNoDuplicateRandom()
	{
		int lenght = displayBlockFile.text.Split ('\n').Length;
		HashSet<int> numbers = new HashSet<int>();
		while (numbers.Count < lenght) 
		{
			numbers.Add(Random.Range (0, lenght));
		}

		noDupNumber = new int[numbers.Count];

		int id = 0;

		foreach (int num in numbers) 
		{
			noDupNumber [id] = num;
			id++;
		}

		if (noDupNumber [0] == lastIndex)
			GenerateNoDuplicateRandom ();
	}

	public void ResetRow()
	{
		#if (UNITY_5_2 || UNITY_5_1 || UNITY_5_0)
			Application.LoadLevel(Application.loadedLevel);
		#else // UNITY_5_3 or above
			UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
		#endif
	}

	public void SetScore()
	{
		mGameController.GotOne();
	}
}
